import json
from typing import List, Dict

from src.models.offer import Offer, ItemCondition, Action


def read(path: str):
    with open(path, mode="r", encoding="utf-8") as fp:
        s = fp.read()
    return s


def read_json(path: str):
    return json.loads(read(path))


def read_offers(path: str) -> List[Offer]:
    objs: List[Dict] = read_json(path)
    result: List[Offer] = []
    for o in objs:
        name = o["name"]
        conds = [ItemCondition(**i) for i in o["conditions"]]
        acts = [Action(**i) for i in o["actions"]]
        result.append(Offer(name=name, conditions=conds, actions=acts))
    return result
