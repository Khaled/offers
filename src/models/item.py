from dataclasses import dataclass
from typing import Optional


@dataclass
class Item:
    item_id: int
    item_type: str
    original_price_per_unit: float
    offer_price_per_unit: float
    condition_offer_id: Optional[int] = None
    action_offer_id: Optional[int] = None
