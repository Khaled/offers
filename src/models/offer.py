from dataclasses import dataclass, field
from typing import List

from src.models.item import Item


@dataclass
class ItemCondition:
    item: str
    quantity: int


@dataclass
class Action:
    quantity: int
    item: str
    reductionValue: float


@dataclass
class Offer:
    name: str
    conditions: List[ItemCondition]
    actions: List[Action]

    @property
    def order(self) -> int:
        """The number of items conditioned on in that offer."""
        o = 0
        for i in self.conditions:
            o += i.quantity
        return o

    def __str__(self) -> str:
        condition_parts = []
        for c in self.conditions:
            condition_parts.append(f"({c.quantity} x {c.item})")
        action_parts = []
        for a in self.actions:
            action_parts.append(f"({a.quantity} x {a.item} [-{a.reductionValue:2.2%}])")

        return "- " + self.name + "\n" + ", ".join(condition_parts) + " → " + ", ".join(action_parts)


@dataclass
class OfferRecord:
    offer_id: int
    offer: Offer
    condition_items: List[Item] = field(default_factory=list)
    action_items: List[Item] = field(default_factory=list)
