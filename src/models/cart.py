import json
import logging
from collections import defaultdict
from copy import deepcopy
from dataclasses import dataclass, asdict
from typing import Dict, Set, DefaultDict, List, Union

from src.models.item import Item
from src.models.offer import Offer, OfferRecord

logging.basicConfig(filename='run.log', level=logging.DEBUG)

logger = logging.getLogger(__name__)


@dataclass
class Cheque:
    subtotal: float
    discount: float
    total: float


class Cart:
    def __init__(self, prices: Dict[str, float], offers: List[Offer]):
        logger.debug(f"Initializing Cart with parameters:")
        logger.debug(f"prices={json.dumps(prices, indent=2)}")
        logger.debug(f"offers={json.dumps(list(map(asdict, offers)), indent=2)}")

        self.cart: DefaultDict[str, List[Item]] = defaultdict(list)
        self.prices = prices
        self.item_kinds: Set[str] = set()
        self.quantities: DefaultDict[str, int] = defaultdict(int)
        self.offers = offers

        # ensure that offers are sorted
        self.offers.sort(key=lambda x: x.order, reverse=True)
        logger.debug(f"sorted offers={json.dumps(list(map(asdict, self.offers)), indent=2)}")

    def add(self, item: Union[str, List[str]]):
        logger.debug(f"Adding {json.dumps(item, indent=2)} to the cart.")
        if type(item) == list:
            for i in item:
                self.add(i)
            return
        if item not in self.prices:
            logger.error(f"Unknown item: {item}")
            return
        self.item_kinds.add(item)
        self.quantities[item] += 1
        item_id = len(self.cart)
        price_per_unit = self.prices[item]
        self.cart[item].append(
            Item(item_id=item_id,
                 item_type=item,
                 original_price_per_unit=price_per_unit,
                 offer_price_per_unit=price_per_unit
                 )
        )

    def offer_applies(self, o: Offer) -> bool:
        for cond in o.conditions:
            if cond.item not in self.checkout_item_kinds:
                return False
            if self.checkout_quantities[cond.item] < cond.quantity:
                return False
        for a in o.actions:
            if a.item not in self.checkout_item_kinds:
                return False
            if self.checkout_quantities[a.item] < a.quantity:
                return False
        logger.debug(f"Offer {o.name} applies!")
        return True

    def apply_offer(self, offer_record: OfferRecord):
        condition_items = []
        action_items = []
        o = offer_record.offer

        for c in o.conditions:
            counter = 0
            for i in self.cart[c.item]:
                # only apply the offer id to the right number of items
                if counter == c.quantity:
                    break
                if i.condition_offer_id is None:
                    i.condition_offer_id = offer_record.offer_id
                    counter += 1
                    condition_items.append(i)

                    # reduce the number of items so when the number is less than
                    # the expected it no longer triggers
                    self.checkout_quantities[c.item] -= 1
                    if self.checkout_quantities[c.item] == 0:
                        self.checkout_quantities.pop(c.item)
                        self.checkout_item_kinds.remove(c.item)

        for a in o.actions:
            counter = 0
            for i in self.cart[a.item]:
                # only apply the offer id to the right number of items
                if counter == a.quantity:
                    break
                if i.action_offer_id is None:
                    i.action_offer_id = offer_record.offer_id

                    # apply offer
                    i.offer_price_per_unit = (1 - a.reductionValue) * i.original_price_per_unit
                    logger.debug(
                        f"Applied offer: {offer_record.offer.name} [{offer_record.offer_id}] on item {json.dumps(asdict(i))}")
                    logger.debug(f"Reducing price from {i.original_price_per_unit:2.2} to {i.offer_price_per_unit:2.2}")

                    counter += 1
                    action_items.append(i)

                    self.checkout_quantities[a.item] -= 1
                    if self.checkout_quantities[a.item] == 0:
                        self.checkout_quantities.pop(a.item)
                        self.checkout_item_kinds.remove(a.item)
        return offer_record

    def checkout(self) -> Cheque:
        self.checkout_quantities = deepcopy(self.quantities)
        self.checkout_item_kinds = deepcopy(self.item_kinds)
        self.offer_records = {}

        matched_offers = []

        i = 0
        # same offer can be applied multiple times
        # only move to the next offer when the current offer can no longer be applied
        while i < len(self.offers):
            offer = self.offers[i]
            if self.offer_applies(offer):
                offer_id = len(self.offer_records)
                offer_record = OfferRecord(offer_id=offer_id, offer=offer)
                self.apply_offer(offer_record)
                matched_offers.append(offer)
            else:
                i += 1

        subtotal = 0
        discounted = 0
        for product, items in self.cart.items():
            for i in items:
                subtotal += i.original_price_per_unit
                discounted += i.offer_price_per_unit

        diff = subtotal - discounted
        print(f"Subtotal: £{subtotal:>8.3}")
        if matched_offers:
            for offer in matched_offers:
                print(offer)
            print(f"-£{diff:>8.3}")
        else:
            print("(No offers available)")
        print(f"Total   : £{discounted:>8.3}")

        return Cheque(subtotal=subtotal, discount=diff, total=discounted)
