import logging
import os
import unittest

from src.models.cart import Cheque, Cart
from src.utils import read_json, read_offers

logging.basicConfig(filename='test.log', level=logging.DEBUG)

logger = logging.getLogger(__name__)


class TestCases(unittest.TestCase):
    def test_runner(self):
        test_cases = os.listdir("resources")

        for case in test_cases:
            logger.debug(f"Running {case} ...")
            logger.debug("-" * 80)
            offers = read_offers(f"resources/{case}/offers.json")
            cart_items = read_json(f"resources/{case}/cart.json")
            prices = read_json(f"resources/{case}/prices.json")
            expected = read_json(f"resources/{case}/expected.json")
            expected_cheque = Cheque(**expected)

            cart = Cart(prices=prices, offers=offers)
            cart.add(cart_items)
            cheque = cart.checkout()

            self.assertAlmostEqual(cheque.subtotal, expected_cheque.subtotal)
            self.assertAlmostEqual(cheque.discount, expected_cheque.discount)
            self.assertAlmostEqual(cheque.total, expected_cheque.total)
            logger.debug("=" * 80)


if __name__ == '__main__':
    unittest.main()
