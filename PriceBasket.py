import argparse

from src.models.cart import Cart
from src.utils import read_offers, read_json


def main():
    parser = argparse.ArgumentParser("Compute the price of a basket")
    parser.add_argument("-p", "--prices", required=False, default="resources/prices.json",
                        help="Path to prices json file")
    parser.add_argument("-o", "--offers", required=False, default="resources/offers.json",
                        help="Path to offers json file")
    parser.add_argument("items", nargs="*", help="Items in the basket")
    args = parser.parse_args()

    prices = read_json(args.prices)
    offers = read_offers(args.offers)

    cart = Cart(prices=prices, offers=offers)

    cart.add(args.items)
    cart.checkout()


if __name__ == '__main__':
    main()
