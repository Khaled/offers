# Offers Project

## Running the code

```shell
python .\PriceBasket.py Apples Milk Bread
```

### Arguments

```text
-p, --prices
    Path for prices json file that contains the prices of each item
-o, --offers
    Path for the offers json file that contains the details of available offers
```

### Example Prices JSON

```json
{
  "Soup": 0.65,
  "Bread": 0.80,
  "Milk": 1.30,
  "Apples": 1.0
}
```

### Example Offers JSON

```json
[
  {
    "name": "Apples have 10% off their normal price this week",
    "conditions": [
      {
        "quantity": 1,
        "item": "Apples"
      }
    ],
    "actions": [
      {
        "quantity": 1,
        "item": "Apples",
        "reductionValue": 0.1
      }
    ]
  },
  {
    "name": "Buy 2 tins of soup and get a loaf of bread for half price",
    "conditions": [
      {
        "quantity": 2,
        "item": "Soup"
      }
    ],
    "actions": [
      {
        "quantity": 1,
        "item": "Bread",
        "reductionValue": 0.5
      }
    ]
  }
]
```

## Tests

Test cases are stored in `test/resources`. Each directory contains the parameters and the expectations of each test
case.

`test/test_offers.py` will iterate through the different cases, load the state for prices, offers, and cart items, run
them through the checkout process, and compare the cheque with the expected cheque.

## Notes

1. There can be multiple ways to apply a set of offers to a set of items. I use the heuristic that offers that rely on a
   bigger quantity of items should be matched first. (By sorting the offers by their `order` while initializing)

2. Using the code is thread-safe, parallelizing the code should be as straightforward as just
   using `multiprocessing.Pool`

